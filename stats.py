class Module:
    def __init__(self):
        self.grades = dict()

    def add_student(self, lastname, firstname, grades):
        self.grades[(lastname, firstname)] = grades

    def n_students(self):
        return len(self.grades)

    def overall_grade(self, lastname, firstname):
        g = self.grades[(lastname, firstname)]
        total = float(sum(g))
        n_grades = len(g)
        return total / n_grades
